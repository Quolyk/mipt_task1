//
//  Animal.m
//  Task1
//
//  Created by Dmitry Venikov on 22/09/14.
//  Copyright (c) 2014 Dmitry Venikov. All rights reserved.
//

#import "Animal.h"

@implementation Animal

- (instancetype)initWithName:(NSString *)name hungerLevel:(NSUInteger)hungerLevel friends:(NSArray *)friends
{
    self = [super init];
    if (self) {
//      FIXME: is it ok to have name == nil, hungerLevel < 0 and friends == nil?
        _name = name;
        _hungerLevel = hungerLevel;
        _friends = friends;
    }
    return self;
}

- (void)setName:(NSString *)name
{
    _name = name;
    NSLog(@"Animal got a new name!");
}

- (void)setHungerLevel:(NSUInteger)hungerLevel
{
    _hungerLevel = hungerLevel;
    if (hungerLevel < 5) {
//      FIXME: it is ok, but show me how you can change thing or not change them according to object state(other properties)
        NSLog(@"Feed the animal");
    }
}

@end
