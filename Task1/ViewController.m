//
//  ViewController.m
//  Task1
//
//  Created by Dmitry Venikov on 22/09/14.
//  Copyright (c) 2014 Dmitry Venikov. All rights reserved.
//

#import "ViewController.h"
#import "Animal.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    Animal *firstAnimal = [[Animal alloc] init];
    firstAnimal.name = @"Dog";
    Animal *secondAnimal = [[Animal alloc] init];
    [secondAnimal setName:@"Cat"];
    Animal *thirdAnimal = [[Animal alloc] init];
    [thirdAnimal setHungerLevel:1];
    Animal *myAnimal = [[Animal alloc] initWithName:@"Mouse"
                                        hungerLevel:6
                                            friends:@[firstAnimal, secondAnimal, thirdAnimal]];
    myAnimal.name = [firstAnimal name];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
