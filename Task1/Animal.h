//
//  Animal.h
//  Task1
//
//  Created by Dmitry Venikov on 22/09/14.
//  Copyright (c) 2014 Dmitry Venikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject

// Everything concerning actions with animals is done in viewDidLoad method of ViewController

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSUInteger hungerLevel;
@property (nonatomic, strong) NSArray *friends;

- (instancetype)initWithName:(NSString *)name
                 hungerLevel:(NSUInteger)hungerLevel
                     friends:(NSArray *)friends;

@end
